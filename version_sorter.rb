require 'strscan'

module VersionSorter
  extend self

  def sort(list)
    ss     = StringScanner.new ''
    pad_to = 0
    list.each { |li| pad_to = li.size if li.size > pad_to }

    list.sort_by do |li|
      ss.string = li
      parts     = ''

      begin
        if match = ss.scan_until(/\d+|[a-z]+/i)
          parts << match.rjust(pad_to)
        else
          break
        end
      end until ss.eos?

      parts
    end
  end

  def rsort(list)
    sort(list).reverse!
  end
end

puts

if $0 == __FILE__
  require 'test/unit'

  class VersionSorterTest < Test::Unit::TestCase
    include VersionSorter

    VERSIONS = %w( 1.0.9b 1.0.9 1.0.10 1.10.1 yui3-999 2.0 1.9.1 yui3-990 3.1.4.2 1.0.9a )
    EXPECTED = %w( 1.0.9 1.0.9a 1.0.9b 1.0.10 1.9.1 1.10.1 2.0 3.1.4.2 yui3-990 yui3-999 )

    def test_sorts_verisons_correctly
      assert_equal EXPECTED, sort(VERSIONS)
    end

    def test_reverse_sorts_verisons_correctly
      assert_equal EXPECTED.reverse, rsort(VERSIONS)
    end
  end

  require 'benchmark'
  versions = IO.read('tags.txt').split("\n")
  count = 10
  Benchmark.bm(20) do |x|
    x.report("sort")  { count.times { VersionSorter.sort(versions)  } }
    x.report("rsort") { count.times { VersionSorter.rsort(versions) } }
  end

  begin
    require 'rubygems'
    require 'ruby-prof'
    result = RubyProf.profile do
      VersionSorter.sort(versions)
    end

    printer = RubyProf::GraphPrinter.new(result)
    printer.print(STDOUT, 0)
  rescue LoadError
    nil
  end

  puts
end
